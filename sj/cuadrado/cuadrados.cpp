#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    char letra = 97;

    int l;

    printf ("Lado: ");
    scanf ("%i", &l);

    for (int f=0; f<l; f++) {
        for (int c=0; c<l; c++){
	    if (c < l / 2)
	    	printf("%c", letra);
	    else
            	printf ("%c", letra+1);
	}
        printf ("\n");
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
