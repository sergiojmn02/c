#include <stdio.h>
#include <stdlib.h>

int main () {

    int l;

    printf ("Lado: ");
    scanf ("%i", &l);

    for (int f=0; f<l; f++) {
        for (int c=0; c<(l-f); c++){
            printf ("*");
	}
        printf ("\n");
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
