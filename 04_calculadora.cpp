#include <stdio.h>
#include <stdlib.h>


int suma (int op1, int op2) {
    return op1 + op2;
}

int resta (int op1, int op2) {
    return op1 - op2;
}

int preguntar_op () {
    static int nop = 0;
    int op;

    printf("Operando %i: ", ++nop);
    scanf(" %i", &op);

    return op;
}

int main() {

    system("clear");

     /* DECLARACIÓN DE VARIABLES */
    int op1,      // Operando 1
        op2,      // Operando 2
        result;



    /* ENTRADA DE DATOS */
    op1 = preguntar_op ();
    op2 = preguntar_op ();

    /* CÁLCULOS */
    result = suma (op1, op2);


    /* SALIDA DE DATOS */
    printf("%i + %i = \033[0;31m\t%i\n\033[0m", op1, op2, result);
    printf("%i + %i = \033[1;36m%08i\n\033[0m", op1, op2, result);
    printf("%i + %i = %8d\n", op1, op2, result);
//    printf("%i + %i = %8.4lf\n", op1, op2, result);


    return EXIT_SUCCESS;

}
