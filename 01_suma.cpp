#include <stdio.h>
#include <stdlib.h>

int globl = 0xDEADBEEF;

int main() {

    int op1, op2, result;

    op1 = 7;
    op2 = 9;
    result = op1 + op2;

    printf ("%i + %i = %i\n", op1, op2, result);

    return EXIT_SUCCESS;

}
