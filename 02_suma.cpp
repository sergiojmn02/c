#include <stdio.h>
#include <stdlib.h>

int globl = 0xDEADBEEF;

int main()
{
    /****************************/
    /* DECLARACIÓN DE VARIABLES */
    /****************************/
    int op1, //Operando 1
       	op2, //Operando 2
       	result;


    /********************/
    /* ENTRADA DE DATOS */
    /********************/
    printf("Primer Número →  ");
    scanf("%i", &op1);

    printf("Segundo Número →  ");
    scanf("%i", &op2);


    /************/
    /* CÁLCULOS */
    /************/
    result = op1 + op2;


    /*******************/
    /* SALIDA DE DATOS */
    /*******************/
    printf("%i + %i = %i\n", op1, op2, result);


    return EXIT_SUCCESS;
}
